﻿CREATE view StatusMappingDetails as
select
	mr8s.CodeNo MR8StatusId, mr8s.CodeName MR8StatusName, nss.StatusId NetSuiteStatusId, nss.StatusName NetSuiteStatusName, sm.IsRush
from
	dbo.Code mr8s with(nolock) join
	MR8StatusMapping sm with(nolock) on mr8s.CodeNo = sm.MR8StatusId join
	NetSuiteStatus nss with(nolock) on sm.NetSuiteStatusId = nss.StatusId