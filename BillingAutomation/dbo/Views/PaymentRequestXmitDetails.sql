﻿CREATE VIEW PaymentRequestXmitDetails as
select
	chk.IssueNo SystemId, sod.SalesOrderId, chk.LocNo VendorId,
	chk.ChkAmt, chk.ChkDate, chk.Memo,
	case
		when chk.Memo like 'C %' then 'CALI'
		when chk.Memo = 'C' then 'CALI'
		else 'HSTN'
	end PaymentCode
from
	mr8.dbo.Checks chk with(nolock) join
	SalesOrderDetails sod on sod.TrackingNo = chk.TrackingNo left join
	PaymentRequestXmit prx on chk.IssueNo = prx.SystemId
where
	prx.NetSuiteId is null and
	(chk.ChkNo is null or len(rtrim(chk.ChkNo)) = 0) and
	sod.RecordType = 'Client'