﻿
CREATE view [dbo].[CasePartiesClientPref] as

select f.SvcNo,d.FirmNo from Cases a (nolock)
inner join CasesParties b (nolock) on a.CaseNo = b.CaseNO
inner join Contacts c (nolock) on b.ContactNo = c.ContactNO
inner join Firms d (nolock) on c.FirmNo = d.FirmNo
inner join MR8..CasesPartiesSvc e (nolock) on b.CasePartyNo = e.CasePartyNo
inner join svcMst f (nolock) on e.SvcNo = f.SvcNo

