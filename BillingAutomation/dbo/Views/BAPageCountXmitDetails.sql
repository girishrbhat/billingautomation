﻿

CREATE view [dbo].[BAPageCountXmitDetails] as
select Itm.ItemNo as SystemId, Itm.RecType as [RecordTypeId], Pages as [Page Count], NoRec Undo  , 
PartNo [Part #], CaseNo[Case #]
from Items Itm with(nolock)
--inner join Code RecdType with(nolock) on Itm.RecType = RecdType.CodeNo
inner join Tracking t with(nolock)  on t.TrackingNo = Itm.TrackingNo
left join BAPageCountXmit x on Itm.ItemNo = x.SystemId and x.NetSuiteId is not null
where x.SystemId is null





