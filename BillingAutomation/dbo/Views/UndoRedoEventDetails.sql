﻿CREATE VIEW UndoRedoEventDetails AS
select
	bex.SalesOrderId, bex.SystemId, dep.Publish, undo.Event, undo.EventDateTime, undo.XmitDateTime,
	ROW_NUMBER() OVER (PARTITION BY undo.SystemId ORDER BY undo.EventDateTime desc) EventRowNum
from
	BillableEventXmit bex join
	mr8.dbo.Depository dep on dep.DpstNo = bex.SystemId join
	mr8.dbo.Items i on i.ItemNo = dep.ItemNo left join
	PublishEventUndoRedo undo on undo.SystemId = i.ItemNo
where
	bex.XmitDateTime is not null and
	bex.Event = 'Publish'