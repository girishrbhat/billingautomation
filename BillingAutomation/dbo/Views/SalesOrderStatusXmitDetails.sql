﻿CREATE view [dbo].[SalesOrderStatusXmitDetails] as
select
	sox.SystemId SalesOrderSystemId, sosx.StatusId NetSuiteStatusId, sosx.StatusDateTime, sosx.SystemId StatusSystemId
from
	SalesOrderStatusXmit sosx join
	SalesOrderXmit sox on sosx.SalesOrderId = sox.SalesOrderId
where
	sosx.XmitDateTime is null and
	sox.XmitDateTime is not null