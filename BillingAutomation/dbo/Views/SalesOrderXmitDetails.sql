﻿CREATE view [dbo].[SalesOrderXmitDetails] as
select distinct  
	x.SalesOrderId, 
	f.FirmNo as CustomerId,
	bc.DisplayName as [Bill To Contact], 
	bc.Email [Bill to Contact Email], 
	StepDueDate as [Tran Date], 
	oc.FirmNo [Sold To(Ordered By)], 
	oc.BillRateGroup [Billing Rate Group], 
	(select top 1 CliMatNo from CasesParties cpc with(nolock) where cpc.CaseNo = cp.CaseNo and  isnull(CliMatNo,'') <> ''  ) [Client Matter #], 
	(select top 1 ClaimNo from CasesParties clm with(nolock) where clm.CaseNo = cp.CaseNo and  isnull(ClaimNo,'') <> ''  ) [Claim Number], 
	oc.DisplayName Requestor, 
	t.CaseNo[Case #], 
	c.CauseNo[Cause No],
	c.CaseName [Cause Style], 
	c.CauseNo + ' ' + c.CaseName [Cause #/Name], 
	c.Patient[Records Pert To], 
	(select top 1 Insured from CasesParties ins with(nolock) where ins.CaseNo = cp.CaseNo and  isnull(Insured,'') <> ''  ) Insured ,
	case
		when st.StateNo is not null then st.StateName
		else locState.StateName 
	end [State],
	t.Instructions [Part Instruction],
	fsvcMst.svcNo as [Client Preference],
	t.LocNo Location,
	locState.StateAbbr [Location State],
	smd.NetSuiteStatusName CurrentStatus,
	smd.NetSuiteStatusId CurrentStatusId,
	x.TrackingNo,
	x.SystemId,
	cpcp.svcNo as [CaseParties ClientPreference],
	rec.CodeName as [Record Type],
	cp.LossDate as [Date of Loss],
	billcomm.CodeName [Billing CommunicationType]
from 
	SalesOrderXmit x join
	Tracking t with(nolock) on t.TrackingNo = x.TrackingNo join
	Cases c with(nolock) on t.CaseNo = c.CaseNo	join 
	(select distinct BillContactNo, CaseNo, LossDate from CasesParties with(nolock) where isscheduling = 1) cp on c.CaseNo = cp.CaseNo join 
	Contacts bc with(nolock) on cp.BillContactNo = bc.ContactNo	join 
	Firms f with(nolock) on bc.FirmNo = f.FirmNo join 
	Contacts oc with(nolock) on c.OrdContactNo = oc.ContactNo left join 
	[State] st with(nolock) on  c.StateNo =  st.StateNo left join
	
	(select FirmNo, fsvc.svcNo from FirmsSvc fsvc with(nolock) inner join SvcMst with(nolock) on fsvc.SvcNo = svcMst.SvcNo and svcMst.SvcSubGroupNo = 1015) as fsvcMst on f.FirmNo = fsvcMst.FirmNo join
	 
	Locations l with(nolock) on t.LocNo = l.LocNo join 
	[state] locState with(nolock) on l.StateNo = locState.StateNo join
	Code ps with(nolock) on t.Status = ps.CodeNo left join 
	StatusMappingDetails smd on t.status = smd.MR8StatusId left join 
	CasePartiesClientPref cpcp (nolock) on f.FirmNO = cpcp.FirmNo	left join
	code rec (nolock) on t.RecType  = rec.CodeNo left join
	Code billcomm (nolock) on f.StmtType = billcomm.CodeNo left join
	SalesOrderHash soh on x.SalesOrderId = soh.SalesOrderId
where 
	x.XmitDateTime is null or
	x.ErrorMessage is not null or
	soh.CompareDate > x.XmitDateTime