﻿CREATE VIEW AvailableLineItems AS
-- note that the row_number function allows us to insure that we're only displaying the line items
-- one time per sales order should we ever have a moment where there are two un-transmitted BEs
-- for any given SO
select
	sod.SystemId SalesOrderSystemId, lix.SystemId, lix.ItemId, lix.ItemCode
from
	LineItemXmit lix join
	(select SalesOrderId, ROW_NUMBER() OVER (PARTITION BY bex.SalesOrderId ORDER BY SystemId) BillableEventRow from BillableEventXmit bex where NetSuiteId is null) bex on lix.SalesOrderId = bex.SalesOrderId join
	SalesOrderDetails sod on lix.SalesOrderId = sod.SalesOrderId left join
	SalesOrdersWithNonBillableCancel nbc on nbc.SalesOrderId = sod.SalesOrderId left join
	mr8.dbo.TrackingLog tl with(nolock) on lix.SystemId = tl.TrackingLogNo
where
	tl.CancelDate is null and		-- received types should not transmit if they are cancelled
	lix.NetSuiteId is null and		-- already transmitted should be excluded
	bex.BillableEventRow = 1 and 	-- only associate with the first available BE
	nbc.SalesOrderId is null		-- hide line items for sales orders that have a non-billable cancel note attached