﻿CREATE VIEW SalesOrdersWithNonBillableCancel as
select
	sox.SalesOrderId
from
	SalesOrderXmit sox join
	mr8.dbo.TrackingLog tl on sox.TrackingNo = tl.TrackingNo and tl.RcvdType = 7070 and tl.CancelDate is null
where
	sox.SalesOrderType = 'Client'