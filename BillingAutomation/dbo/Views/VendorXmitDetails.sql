﻿CREATE view [dbo].[VendorXmitDetails] as
select distinct
	loc.LocNo VendorId,	loc.LocName Name, loc.Address, 
	loc.City, st.StateAbbr, loc.PostCode ZipCode,
	loc.MainPhone PhoneNumber, loc.Fax FaxNumber, 'Custodian' LocType
from
	Locations loc join
	"State" st on loc.StateNo = st.StateNo	left join 	
	VendorXmit xmit on loc.LocNo = xmit.VendorId and xmit.NetSuiteId is not null left join
	VendorHash vh on loc.locno = vh.VendorId join
	Tracking t on t.locno = loc.locno
where 
	(
	xmit.XmitDateTime is null or
	xmit.ErrorMsg is not null or
	xmit.XmitDateTime < vh.CompareDate
	) and
	t.Entered >= '2015-01-01'