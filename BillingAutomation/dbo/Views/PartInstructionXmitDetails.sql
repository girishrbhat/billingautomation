﻿CREATE view [dbo].[PartInstructionXmitDetails] as
select 
	codeNo SystemId, CodeName [Part Instruction], Disabled IsDisabled 
from 
	Code with(nolock) left join
	PartInstructionXmit pix on pix.SystemId = code.CodeNo
where 
	CodeGroupNo = 802 and --and Disabled = 0
	(pix.NetSuiteId is null or pix.ErrorMsg is not null)