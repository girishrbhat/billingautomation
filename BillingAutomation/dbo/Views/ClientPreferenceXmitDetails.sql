﻿CREATE view [dbo].[ClientPreferenceXmitDetails] as
select 
	svcNo as SystemId, svcName as [Client Preference Name], Disabled IsDisabled 
from 
	svcMst with(nolock) left join
	ClientPreferenceXmit cpx on svcMst.svcNo = cpx.SystemId
where 
	SvcSubGroupNo = 1015 and
	(cpx.NetSuiteId is null or cpx.ErrorMsg is not null)