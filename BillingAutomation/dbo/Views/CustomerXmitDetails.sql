﻿CREATE view [dbo].[CustomerXmitDetails] as
select distinct
	f.FirmNo CustomerId, f.FirmName Name, f.Address,
	f.City, st.StateAbbr, f.PostCode ZipCode,
	f.MainPhone PhoneNumber, f.Fax FaxNumber, f.Email EmailAddress,
	t.CodeName Term, c.CodeName CustCategoryVerbose,
	case 
		when c.CodeName like '%opposing%' then 'Opposing Counsel'
		when c.CodeName like '%firm%' then 'Firm'
		else 'Carrier'
	end CustCategory,
	BillRtGrp.CodeName as BillRateGroup,
	BillRtGrp.CodeNo as BillRateGroupId,
	SvcMst.svcName as ClientPreference ,
	SvcMst.SvcNo as ClientPreferenceId,
	f.[RefAcctNo] as [Invoice Format]
from
	Firms f join
	"State" st on f.StateNo = st.StateNo left join 	
	Code c on c.CodeNo = f.FirmType left join  
	Code t on f.Term = t.CodeNo left join 
	Code BillRtGrp on f.BillRateGroup = BillRtGrp.CodeNo left join
	FirmsSvc svc on  f.FirmNo = svc.FirmNo left join 
	SvcMst SvcMst on svc.SvcNo =  SvcMst.SvcNo left join
	CustomerHash h on f.firmno = h.CustomerId left join
	CustomerXmit custx on f.firmno = custx.CustomerId join
	mr8.dbo.Contacts con on con.FirmNo = f.firmno join
	mr8.dbo.Cases cas on cas.OrdContactNo = con.ContactNo
where
	(
	1=1 or
	custx.XmitDateTime is null or
	custx.ErrorMsg is not null or
	h.CompareDate > custx.XmitDateTime
	) and
	cas.Entered >= '2015-01-01' and 
	case when (isnull(svcMst.svcNo, '')  <> '') then SvcSubGroupno else 1 end = case when (isnull(svcMst.svcNo, '')  <> '') then 1015 else 1 end