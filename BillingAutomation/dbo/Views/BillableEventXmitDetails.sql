﻿CREATE view [dbo].[BillableEventXmitDetails] as
-- all of the real publish events
select distinct
	sod.SystemId SalesOrderSystemId, bex.SystemId, bex.PartStatus,
	ft.CodeName FileType, sod.RecordType, i.Pages as Quantity, dep.Entered PublishDate,
	rt.CodeName RecordTypeName
from
	BillableEventXmit bex join
	mr8.dbo.Depository dep on bex.SystemId = dep.DpstNo and bex.Event = 'Publish' join
	mr8.dbo.Items i on dep.itemno = i.ItemNo join
	SalesOrderDetails sod on i.TrackingNo = sod.TrackingNo left join
	SalesOrdersWithNonBillableCancel nbc on nbc.SalesOrderId = sod.SalesOrderId join
	mr8.dbo.Code ft on dep.FileType = ft.CodeNo join
	mr8.dbo.Code rt on sod.RecordType = rt.CodeNo
where
	bex.NetSuiteId is null and
	nbc.SalesOrderId is null and								-- hide line items for sales orders that have a non-billable cancel note attached
	(sod.SalesOrderType = 'OC' and dep.FileType not in (6721))	-- if it is an OC sales order we do NOT publish for a Rejection
	
union

-- cancel and finalize events
select distinct
	sod.SystemId SalesOrderSystemId, bex.SystemId, bex.PartStatus, 
	null FileType, sod.RecordType, 0 Quantity, stax.StatusDateTime PublishDate,
	rt.CodeName RecordTypeName
from
	BillableEventXmit bex join
	SalesOrderDetails sod on bex.SalesOrderId = sod.SalesOrderId join
	SalesOrderStatusXmit stax on bex.SystemId = stax.SystemId and bex.Event in ('Cancel' , 'Finalize') left join
	SalesOrdersWithNonBillableCancel nbc on nbc.SalesOrderId = sod.SalesOrderId join
	mr8.dbo.Code rt on sod.RecordType = rt.CodeNo
where
	bex.NetSuiteId is null and 
	nbc.SalesOrderId is null		-- hide line items for sales orders that have a non-billable cancel note attached