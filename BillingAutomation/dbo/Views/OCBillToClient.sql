﻿CREATE view OCBillToClient as
select distinct
	t.CaseNo, cspOC.CasePartyNo, cspOC.ContactNo
from
	mr8.dbo.Tracking t with(nolock) join
	mr8.dbo.CasesParties cspSched with(nolock) on cspSched.CaseNo = t.CaseNo and cspSched.IsScheduling = 1 join
	mr8.dbo.Contacts conSched with(nolock) on cspSched.ContactNo = conSched.ContactNo join
	mr8.dbo.Firms fSched with(nolock) on conSched.FirmNo = fSched.FirmNo join
	mr8.dbo.CasesParties cspOC with(nolock) on cspOC.CaseNo = t.CaseNo and cspOC.IsScheduling = 0 and cspOC.IsOrdering = 0 join
	mr8.dbo.Contacts conOC with(nolock) on cspOC.ContactNo = conOC.ContactNo join
	mr8.dbo.Firms fOC with(nolock) on conOC.FirmNo = fOC.FirmNo and fOC.FirmType <> 7051 left join
	mr8.dbo.CasesPartiesLog cspOCLog with(nolock) on cspOCLog.CasePartyNo = cspOC.CasePartyNo left join
	mr8.dbo.FirmsTag ocTag with(nolock) on ocTag.FirmNo = fOC.FirmNo left join
	mr8.dbo.Code tag with(nolock) on ocTag.TagName = tag.CodeName and tag.CodeNo = 7053
where											-- we are excluding OCs where
	tag.CodeNo is not null or 					-- the client firm gives copies away
	fSched.StateNo not in (108,133,139,145) or	-- the client firm is in one of the free states
	ocTag.TagName is not null and				-- the OC is receiving free copies on this case
	t.Entered >= '2015-01-01'