﻿CREATE view [dbo].[SalesOrderDetails] as
-- client sales orders
select distinct
	t.TrackingNo, t.CaseNo, t.PartNo, sox.SalesOrderId, sox.SystemId, 
	tl.Remarks, tl.RcvdType, code.CodeNo, code.CodeName, tl.RcvdDate, tl.Entered,
	smd.NetSuiteStatusId, smd.NetSuiteStatusName, t.RecType RecordType,
	sox.SalesOrderType
from
	SalesOrderXmit sox join
	dbo.Tracking t with(nolock) on sox.TrackingNo = t.TrackingNo join
	dbo.Locations loc with(nolock) on t.LocNo = loc.LocNo join
	dbo.TrackingLog tl with(nolock) on t.TrackingNo = tl.TrackingNo and tl.RcvdType = 6781 left join 
	(select distinct trackingno from dbo.Invoices inv with(nolock) where inv.PostDate < '2016-09-01') inv on t.TrackingNo = inv.TrackingNo left join 
	dbo.Code code with(nolock) on tl.RcvdType  = code.CodeNo left join 
	StatusMappingDetails smd on t.status = smd.MR8StatusId
where
	inv.trackingno is null and 
	t.status not in (836)

/*
union

-- we also need to know if we have an OC Sales Order and that will be
-- Case-Part-FirmNo for the Id
select distinct
	t.TrackingNo, t.CaseNo, t.PartNo, 
	cast(t.CaseNo as varchar) + '-'  + cast(t.PartNo as varchar) + '-' + cast(f.FirmNo as varchar) SalesOrderId,
	tl.Remarks, tl.RcvdType, code.CodeNo, code.CodeName, tl.RcvdDate, tl.Entered,
	smd.NetSuiteStatusId, smd.NetSuiteStatusName, t.RecType RecordType
from
	dbo.Tracking t with(nolock) join
	dbo.Locations loc with(nolock) on t.LocNo = loc.LocNo join
	dbo.TrackingLog tl with(nolock) on t.TrackingNo = tl.TrackingNo and tl.RcvdType = 6781 left join 
	(select distinct trackingno from dbo.Invoices inv with(nolock) where inv.PostDate < '2016-09-01') inv on t.TrackingNo = inv.TrackingNo left join 
	dbo.Code code with(nolock) on tl.RcvdType  = code.CodeNo left join 
	StatusMappingDetails smd on t.status = smd.MR8StatusId join
	mr8.dbo.CasesParties csp on csp.CaseNo = t.CaseNo join
	mr8.dbo.Contacts con on csp.ContactNo = con.ContactNo join
	mr8.dbo.Firms f on con.FirmNo = f.FirmNo left join
	OCBillToClient ocb2c on csp.CasePartyNo = ocb2c.CasePartyNo and csp.CaseNo = ocb2c.CaseNo
where
	inv.trackingno is null and 
	t.status not in (836) and
	csp.IsScheduling = 0 and csp.IsOrdering = 0 and		-- scheduling and ordering are for clients
	f.FirmType <> 7051 and								-- experts always are billed to client, no new SO generated
	ocb2c.CasePartyNo is null							-- they don't get free copies
*/