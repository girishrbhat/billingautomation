﻿CREATE TABLE [dbo].[NetSuiteErrors] (
    [Id]    INT           NOT NULL,
    [Name]  VARCHAR (100) NOT NULL,
    [Email] VARCHAR (200) NULL
);

