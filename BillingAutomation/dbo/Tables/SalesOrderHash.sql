﻿CREATE TABLE [dbo].[SalesOrderHash] (
    [SalesOrderId] VARCHAR (64)    NOT NULL,
    [HashData]     VARBINARY (MAX) NULL,
    [CompareDate]  DATETIME        NOT NULL
);

