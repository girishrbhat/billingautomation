﻿CREATE TABLE [dbo].[BillableEventXmit] (
    [SalesOrderId]      VARCHAR (64)   NOT NULL,
    [NetSuiteId]        VARCHAR (64)   NULL,
    [SystemId]          VARCHAR (64)   NOT NULL,
    [XmitDateTime]      DATETIME       NULL,
    [ErrorMsg]          VARCHAR (1000) NULL,
    [ErrorCount]        INT            NULL,
    [LastErrorDateTime] DATETIME       NULL,
    [PartStatus]        INT            NOT NULL,
    [Event]             VARCHAR (16)   NOT NULL
);

