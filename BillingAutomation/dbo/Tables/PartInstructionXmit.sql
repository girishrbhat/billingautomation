﻿CREATE TABLE [dbo].[PartInstructionXmit] (
    [SystemId]          INT            NOT NULL,
    [NetSuiteId]        VARCHAR (32)   NULL,
    [XmitDateTime]      DATETIME       NULL,
    [ErrorCode]         VARCHAR (32)   NULL,
    [ErrorMsg]          VARCHAR (1000) NULL,
    [ErrorCount]        INT            NULL,
    [LastErrorDateTime] DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([SystemId] ASC)
);

