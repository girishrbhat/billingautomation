﻿CREATE TABLE [dbo].[VendorXmit] (
    [VendorId]          INT            NOT NULL,
    [XmitDateTime]      DATETIME       NULL,
    [NetSuiteId]        VARCHAR (32)   NULL,
    [ErrorMsg]          VARCHAR (1000) NULL,
    [ErrorCount]        INT            NULL,
    [LastErrorDateTime] DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([VendorId] ASC)
);

