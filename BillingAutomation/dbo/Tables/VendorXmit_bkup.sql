﻿CREATE TABLE [dbo].[VendorXmit_bkup] (
    [VendorId]          INT           NOT NULL,
    [XmitDateTime]      DATETIME      NULL,
    [NetSuiteId]        VARCHAR (32)  NULL,
    [ErrorCode]         VARCHAR (32)  NULL,
    [ErrorMsg]          VARCHAR (256) NULL,
    [ErrorCount]        INT           NULL,
    [LastErrorDateTime] DATETIME      NULL
);

