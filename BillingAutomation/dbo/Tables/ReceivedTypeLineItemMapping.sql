﻿CREATE TABLE [dbo].[ReceivedTypeLineItemMapping] (
    [ReceivedTypeId] INT NOT NULL,
    [ServiceItemId]  INT NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [ReceivedTypeLineItemMapping_ReceivedTypeId_IDX]
    ON [dbo].[ReceivedTypeLineItemMapping]([ReceivedTypeId] ASC);

