﻿CREATE TABLE [dbo].[CustomerXmit_bkup] (
    [CustomerId]        INT           NOT NULL,
    [NetSuiteId]        VARCHAR (32)  NULL,
    [XmitDateTime]      DATETIME      NULL,
    [ErrorCode]         VARCHAR (32)  NULL,
    [ErrorMsg]          VARCHAR (256) NULL,
    [ErrorCount]        INT           NULL,
    [LastErrorDateTime] DATETIME      NULL
);

