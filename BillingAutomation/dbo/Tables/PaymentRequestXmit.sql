﻿CREATE TABLE [dbo].[PaymentRequestXmit] (
    [SystemId]          INT            NOT NULL,
    [NetSuiteId]        VARCHAR (32)   NULL,
    [XmitDateTime]      DATETIME       NULL,
    [ErrorMessage]      VARCHAR (1000) NULL,
    [ErrorCount]        INT            NULL,
    [LastErrorDateTime] DATETIME       NULL
);

