﻿CREATE TABLE [dbo].[CustomerHash] (
    [CustomerId]   INT             NOT NULL,
    [CustomerData] VARBINARY (MAX) NOT NULL,
    [CompareDate]  DATETIME        NOT NULL
);

