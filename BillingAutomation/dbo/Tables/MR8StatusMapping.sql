﻿CREATE TABLE [dbo].[MR8StatusMapping] (
    [MR8StatusId]      INT NULL,
    [NetSuiteStatusId] INT NULL,
    [IsRush]           BIT DEFAULT ((0)) NULL
);

