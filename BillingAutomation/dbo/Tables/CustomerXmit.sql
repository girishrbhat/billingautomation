﻿CREATE TABLE [dbo].[CustomerXmit] (
    [CustomerId]        INT            NOT NULL,
    [NetSuiteId]        VARCHAR (32)   NULL,
    [XmitDateTime]      DATETIME       NULL,
    [ErrorMsg]          VARCHAR (1000) NULL,
    [ErrorCount]        INT            NULL,
    [LastErrorDateTime] DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([CustomerId] ASC)
);

