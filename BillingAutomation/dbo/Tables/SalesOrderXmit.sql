﻿CREATE TABLE [dbo].[SalesOrderXmit] (
    [NetSuiteId]        VARCHAR (32)   NULL,
    [ErrorMessage]      VARCHAR (1000) NULL,
    [ErrorCount]        VARCHAR (256)  NULL,
    [LastErrorDateTime] DATETIME       NULL,
    [SalesOrderId]      VARCHAR (64)   NOT NULL,
    [XmitDateTime]      DATETIME       NULL,
    [SystemId]          VARCHAR (64)   NOT NULL,
    [TrackingNo]        INT            NULL,
    [SalesOrderType]    VARCHAR (16)   NULL,
    CONSTRAINT [SalesOrderXmit_PK] PRIMARY KEY CLUSTERED ([SalesOrderId] ASC)
);

