﻿CREATE TABLE [dbo].[PublishEventUndoRedo] (
    [SalesOrderId]  VARCHAR (64) NOT NULL,
    [SystemId]      INT          NOT NULL,
    [EventDateTime] DATETIME     NOT NULL,
    [Event]         VARCHAR (32) NOT NULL,
    [XmitDateTime]  DATETIME     NULL
);

