﻿CREATE TABLE [dbo].[LineItemXmit] (
    [SalesOrderId]      VARCHAR (64)   NOT NULL,
    [SystemId]          INT            NOT NULL,
    [ItemId]            INT            NOT NULL,
    [ItemCode]          VARCHAR (64)   NOT NULL,
    [XmitDateTime]      DATETIME       NULL,
    [ErrorMessage]      VARCHAR (1000) NULL,
    [ErrorCount]        INT            NULL,
    [LastErrorDateTime] DATETIME       NULL,
    [NetSuiteId]        VARCHAR (64)   NULL
);

