﻿CREATE TABLE [dbo].[VendorHash] (
    [VendorId]    INT             NOT NULL,
    [VendorData]  VARBINARY (MAX) NOT NULL,
    [CompareDate] DATETIME        NOT NULL
);

