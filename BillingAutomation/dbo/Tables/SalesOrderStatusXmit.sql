﻿CREATE TABLE [dbo].[SalesOrderStatusXmit] (
    [SalesOrderId]      VARCHAR (64)   NOT NULL,
    [NetSuiteId]        VARCHAR (32)   NULL,
    [StatusDateTime]    DATETIME       NOT NULL,
    [XmitDateTime]      DATETIME       NULL,
    [ErrorMessage]      VARCHAR (1000) NULL,
    [ErrorCount]        INT            NULL,
    [LastErrorDateTime] DATETIME       NULL,
    [SystemId]          INT            NOT NULL,
    [StatusId]          INT            NOT NULL
);

