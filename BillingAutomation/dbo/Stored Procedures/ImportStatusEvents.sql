﻿CREATE PROCEDURE [dbo].[ImportStatusEvents]
AS
BEGIN
	-- import from TrackingStatusLog changes that have not yet been imported
	insert into SalesOrderStatusXmit (SalesOrderId, SystemId, StatusId, StatusDateTime)
	select distinct
		sox.SalesOrderId, tsl.TrackingStatusNo, msm.NetSuiteStatusId, tsl.Entered
	from
		SalesOrderXmit sox join
		mr8.dbo.TrackingStatusLog tsl with(nolock) on sox.TrackingNo = tsl.TrackingNo join
		MR8StatusMapping msm on tsl.Status = msm.MR8StatusId left join
		SalesOrderStatusXmit sx on tsl.TrackingStatusNo = sx.SystemId
	where
		sx.SalesOrderId is null and
		sox.XmitDateTime is not null

	-- import the Finalized received type as a status
	insert into SalesOrderStatusXmit (SalesOrderId, SystemId, StatusId, StatusDateTime)
	select distinct
		sod.SalesOrderId, tl.TrackingLogNo SystemId, 501, tl.Entered
	from
		SalesOrderDetails sod join
		mr8.dbo.TrackingLog tl with(nolock) on sod.TrackingNo = tl.TrackingNo and tl.RcvdType = 7073 left join 
		BillableEventXmit bex on bex.SystemId = tl.TrackingLogNo
	where
		bex.SalesOrderId is null
END