﻿CREATE PROCEDURE [dbo].[ImportBillableEvents]
AS
BEGIN
	-- first we're going to look for Publish events
	insert into BillableEventXmit (SalesOrderId, SystemId, PartStatus, Event)
	select distinct
		sod.SalesOrderId, dep.DpstNo SystemId, sod.NetSuiteStatusId, 'Publish'
	from
		mr8.dbo.Items i join
		mr8.dbo.Depository dep on dep.itemno = i.ItemNo join
		SalesOrderDetails sod on i.TrackingNo = sod.TrackingNo left join
		BillableEventXmit bex on bex.SystemId = dep.DpstNo
	where
		dep.Publish = 1 and dep.Entered >= '2016-09-01' and
		bex.SalesOrderId is null

	-- now we look for a billable cancel event defined as a cancel grouping status (900)
	-- in the absence of the non-billable cancel note
	insert into BillableEventXmit(SalesOrderId, SystemId, PartStatus, Event)
	select distinct
		stax.SalesOrderId, stax.SystemId, 900, 'Cancel'
	from
		SalesOrderStatusXmit stax left join
		BillableEventXmit bex on stax.SystemId = bex.SystemId join
		SalesOrderXmit sox on stax.SalesOrderId = sox.SalesOrderId left join
		SalesOrdersWithNonBillableCancel nbc on sox.SalesOrderId = nbc.SalesOrderId
	where
		stax.SystemId = 900 and
		bex.SystemId is null and
		nbc.SalesOrderId is null
		
	-- import Finalize as a 0 quantity billable event
	insert into BillableEventXmit(SalesOrderId, SystemId, PartStatus, Event)
	select distinct
		sox.SalesOrderId, tl.TrackingLogNo, 501, 'Finalize'
	from
		SalesOrderXmit sox join 
		mr8.dbo.TrackingLog tl with(nolock) on sox.TrackingNo = tl.TrackingNo and tl.RcvdType = 7073 left join 
		BillableEventXmit bex on tl.TrackingLogNo = bex.SystemId
	where
		bex.SystemId is null
		
	-- i need to import the Rush line item here because this is the first time i have a chance to know if
	-- i need it
	insert into LineItemXmit(SalesOrderId, SystemId, ItemId, ItemCode)
	select 
		SalesOrderId, SystemId, ItemId, ItemCode
	from
		(
		select
			bex.SalesOrderId, tsl.TrackingStatusNo SystemId, 1529 ItemId, 'Rush' ItemCode, smd.IsRush, smd.MR8StatusId,
			ROW_NUMBER() OVER (PARTITION BY bex.SystemId ORDER BY tsl.Entered desc) StatusRow
		from
			BillableEventXmit bex join
			SalesOrderXmit sod on bex.SalesOrderId = sod.SalesOrderId and bex.Event = 'Publish' join
			mr8.dbo.TrackingStatusLog tsl with(nolock) on sod.TrackingNo = tsl.TrackingNo join
			StatusMappingDetails smd on tsl.Status = smd.MR8StatusId and smd.NetSuiteStatusId in (300) left join
			LineItemXmit lix on lix.SalesOrderId = bex.SalesOrderId and bex.SystemId = lix.SystemId and lix.ItemId = 1529 
		where
			lix.ItemId is null -- don't want to duplicate rush line items
		) x
	where
		StatusRow = 1 and				-- the most recent In Progress status item
		IsRush = 1 and					-- is a Rush status
		MR8StatusId not in (7068,7087)	-- and is *not* a non-billable Rush

	-- if we receive one of the three "records only" file types then we need to add
	-- a kickback line item here
	insert into LineItemXmit(SalesOrderId, SystemId, ItemId, ItemCode)
	select 
		bex.SalesOrderId, bex.SystemId, 1614 ItemId, 'Kickback' ItemCode
	from
		BillableEventXmit bex join
		mr8.dbo.Depository dep on dep.DpstNo = bex.SystemId left join
		LineItemXmit lix on lix.SalesOrderId = bex.SalesOrderId and bex.SystemId = lix.SystemId and lix.ItemId = 1614
	where
		dep.FileType in (6663,7045,7046) and
		lix.ItemId is null
END