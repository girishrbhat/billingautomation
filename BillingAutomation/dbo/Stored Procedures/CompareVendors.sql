﻿-- =============================================
-- Author:		Girish Bhat
-- Description:	Customer Hash compare program
-- =============================================
CREATE PROCEDURE [dbo].[CompareVendors]
	
AS
BEGIN

create table #Vendors
(
	VendorId int,
	VendorHash varbinary(max)
)

-- Get all the Vendor data and conver it into MD5 to compare
insert into #Vendors
select distinct loc.LocNo, hashbytes('md5',
	cast(loc.LocNo as varchar) + loc.LocName +  loc.Address + loc.City + st.StateAbbr + loc.PostCode + loc.MainPhone + loc.Fax)
from
	mr8.dbo.Locations loc join
	"State" st on loc.StateNo = st.StateNo 

	
merge VendorHash as vh
	using (select VendorId, VendorHash from #Vendors) as v(VendorId,VendorHash)
		on (vh.VendorId = v.VendorId)
	when matched and vh.VendorData <> v.VendorHash then 
		update set VendorData = v.VendorHash, CompareDate = getdate()
	when not matched then
		insert (VendorId,VendorData,CompareDate)
		values(v.VendorId,v.VendorHash,getdate());

END