﻿
CREATE PROCEDURE [dbo].[ImportLineItems] as
BEGIN
	-- TODO exclude OC sales orders from attracting line items
	


	-- Rule 11 Tracking Services 
	insert into LineItemXmit(SalesOrderId, SystemId, ItemId, ItemCode)
	select distinct
		sox.SalesOrderId, tsvc.TrackingSvcNo, 1900, 'Rule 11'
	from
		SalesOrderXmit sox join
		mr8.dbo.TrackingSvc tsvc on tsvc.TrackingNo = sox.TrackingNo and tsvc.SvcNo = 1900 left join
		LineItemXmit lix on lix.SystemId = tsvc.TrackingSvcNo
	where
		lix.SystemId is null and SalesOrderType  ='CLIENT'
	
	-- Line Item Received Types
	insert into LineItemXmit(SalesOrderId, SystemId, ItemId, ItemCode)
	select distinct
		sox.SalesOrderId, tl.TrackingLogNo, rtlim.ServiceItemId, c.CodeName
	from
		SalesOrderXmit sox join
		mr8.dbo.TrackingLog tl on tl.TrackingNo = sox.TrackingNo and tl.CancelDate is null join
		mr8.dbo.Code c on tl.RcvdType = c.CodeNo join
		ReceivedTypeLineItemMapping rtlim on tl.RcvdType = rtlim.ReceivedTypeId left join
		LineItemXmit lix on lix.SystemId = tl.TrackingLogNo
	where
		lix.SystemId is null and SalesOrderType  ='CLIENT'
		
	-- TODO OC Copies Billed to Client
	insert into LineItemXmit(SalesOrderId, SystemId, ItemId, ItemCode)
	select  distinct SalesOrderId, t.TrackingNo, case when CodeName like '%Records Obtained%' then 1559 else 1661 end,
	case when CodeName like '%Records Obtained%' then 'MR Copies' else 'MR No Records Statement' end   from SalesOrderXmit sox
			inner join Tracking t on  sox.TrackingNo = t.TrackingNo
			inner join OCBillToClient oc on t.CaseNo = oc.CaseNo
			inner join Code st on  st.CodeNo = t.Status and  (st.CodeName like '%Records Obtained%' or st.CodeName like '%No Records%')
	where 	SalesOrderType  ='CLIENT'	

	
	-- TODO Expert Copies 
	insert into LineItemXmit(SalesOrderId, SystemId, ItemId, ItemCode)
	select distinct
		sox.SalesOrderId, tl.TrackingLogNo, rcd.CodeNo, rcd.CodeName
	from
		SalesOrderXmit sox join
		tracking t on sox.TrackingNo = t.TrackingNo join
		mr8.dbo.TrackingLog tl on tl.TrackingNo = t.TrackingNo and (tl.CancelDate is null or tl.CancelDate < = getdate()) join
		mr8.dbo.Code rcd on tl.RcvdType = rcd.CodeNo and (CodeName like '%Expert%' ) join
		cases c on 	t.caseno = c.CaseNo 
		join CasesParties cp on cp.caseNo = c.CaseNo 
		inner join Contacts con on cp.ContactNo  = con.ContactNo
		inner join Firms f on con.firmNo = f.FirmNo  and firmType = 7051
	where
		SalesOrderType  ='CLIENT'

END

