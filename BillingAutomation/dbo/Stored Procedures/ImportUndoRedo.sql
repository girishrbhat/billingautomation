﻿CREATE PROCEDURE ImportUndoRedo AS
BEGIN
	-- was there a file that was published and then unpublished
	insert into PublishEventUndoRedo(SalesOrderId, SystemId, EventDateTime, Event)
	select
		SalesOrderId, SystemId, getdate(), 'Undo'
	from
		UndoRedoEventDetails ured
	where
		EventRowNum = 1 and
		(Event is null or Event = 'Redo') and
		Publish = 0
	
	-- was there an UNDO for a file that is now published
	insert into PublishEventUndoRedo(SalesOrderId, SystemId, EventDateTime, Event)
	select
		SalesOrderId, SystemId, getdate(), 'Redo'
	from
		UndoRedoEventDetails ured
	where
		EventRowNum = 1 and
		Event = 'Undo' and
		Publish = 1
END