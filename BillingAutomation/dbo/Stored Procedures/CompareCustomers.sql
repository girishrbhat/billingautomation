﻿-- =============================================
-- Author:		Girish Bhat
-- Description:	Customer Hash compare program
-- =============================================
CREATE PROCEDURE [dbo].[CompareCustomers]
	
AS
BEGIN

create table #Customers
(
	CustomerId int,
	CustomerHash varbinary(max)
)

-- Get all the customer data and conver it into MD5 to compare
insert into #Customers
select distinct f.FirmNo, hashbytes('md5',
	cast(f.FirmNo as varchar) + f.FirmName +  f.Address + f.City + st.StateAbbr + f.PostCode + f.MainPhone +  f.Fax  +  f.Email + t.CodeName +  c.CodeName  +
	case 
		when c.CodeName like '%opposing%' then 'Opposing Counsel'
		when c.CodeName like '%firm%' then 'Firm'
		else 'Carrier'
	end 
	 + 	isnull(BillRtGrp.CodeName,'') + 
	 isnull(fSvcMst.svcName ,''))	 
from
	Firms f join
	"State" st on f.StateNo = st.StateNo left join
	Code c on c.CodeNo = f.FirmType left join
	Code t on f.Term = t.CodeNo left join
	Code BillRtGrp on f.BillRateGroup = BillRtGrp.CodeNo left join
	(select FirmNo, fsvc.svcNo, svcmst.svcName from FirmsSvc fsvc with(nolock) inner join SvcMst with(nolock) on fsvc.SvcNo = svcMst.SvcNo and svcMst.SvcSubGroupNo = 1015) as fsvcMst on f.FirmNo = fsvcMst.FirmNo 

merge CustomerHash as ch
	using (select CustomerId, CustomerHash from #Customers) as c(CustomerId,CustomerHash)
		on (ch.CustomerId = c.CustomerId)
	when matched and ch.CustomerData <> c.CustomerHash then 
		update set CustomerData = c.CustomerHash, CompareDate = getdate()
	when not matched then
		insert (CustomerId,CustomerData,CompareDate)
		values(c.CustomerId,c.CustomerHash,getdate());

END
