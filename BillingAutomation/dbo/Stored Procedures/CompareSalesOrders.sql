﻿CREATE PROCEDURE [dbo].[CompareSalesOrders]
AS
BEGIN
	create table #updateSalesOrder(SalesOrderId varchar(64), HashData varbinary(max), CompareDate datetime)
	
	insert into #updateSalesOrder (SalesOrderId, CompareDate, HashData)
	select distinct
		sod.SalesOrderId, getdate(),
		hashbytes('md5',
				cast(sod."Billing Rate Group" as varchar) + 
				cast(isnull(sod."CaseParties ClientPreference", '') as varchar) +
				cast(sod."Part Instruction" as varchar) + sod."State" + cast(sod."Sold To(Ordered By)" as varchar) +
				sod."Bill To Contact" + cast(sod.Location as varchar)
	 ) HashData
	from
		SalesOrderXmitDetails sod 
		
	update SalesOrderHash set
		HashData = uso.HashData, CompareDate = uso.CompareDate
	from
		SalesOrderHash soh join
		#updateSalesOrder uso on soh.SalesOrderId = uso.SalesOrderId
	where
		soh.HashData <> uso.HashData

	drop table #updateSalesOrder
END