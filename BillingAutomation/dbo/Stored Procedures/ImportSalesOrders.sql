﻿CREATE PROCEDURE [dbo].[ImportSalesOrders]
AS
BEGIN
	-- first we get the sales order entered
	insert into SalesOrderXmit (SalesOrderId, SystemId, TrackingNo, SalesOrderType)
	select distinct
		sod.SalesOrderId, sod.SystemId, sod.TrackingNo, sod.SalesOrderType
	from
		SalesOrderIdDetails sod left join
		SalesOrderXmit sox on sod.SalesOrderId = sox.SalesOrderId 
	where
		sox.SalesOrderId is null 
		and sod.SalesOrderType = 'CLIENT'	-- TODO when everything else is ready for OC sales orders get rid of this

	-- now we have the initial hash value to use to see if something changes later
	insert into SalesOrderHash (SalesOrderId, CompareDate, HashData)
	select distinct
		sod.SalesOrderId, getdate(),
		hashbytes('md5',
				cast(sod."Billing Rate Group" as varchar) + 
				cast(isnull(sod."CaseParties ClientPreference", '') as varchar) +
				cast(sod."Part Instruction" as varchar) + sod."State" + cast(sod."Sold To(Ordered By)" as varchar) +
				sod."Bill To Contact" + cast(sod.Location as varchar)
	 ) HashData
	from
		SalesOrderXmitDetails sod left join
		SalesOrderHash sox on sod.SalesOrderId = sox.SalesOrderId 
	where
		sox.SalesOrderId is null
END